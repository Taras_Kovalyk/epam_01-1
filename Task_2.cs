﻿using System;
using System.Text;
using System.Xml;

public interface IPolynomial
{
    // Indexer for polinomial
    double this[int i] { get; set; }
    // return lenght polinomial
    int GetPowerPolinomial();
    // calculate polinom for input value
    double CalculatePolynomial(int value);
    // print polinom
    StringBuilder PrintPolynomial();
}

public class Polynomial : IPolynomial
{
    private double[] _coefficients;

    public Polynomial(int power)
    {
        _coefficients = new double[power];
    }

    public Polynomial(double[] coefficients)
    {
        _coefficients = coefficients;
    }

    public double this[int i]
    {
        get { return _coefficients[i]; }
        set { _coefficients[i] = value; }
    }

    public int GetPowerPolinomial()
    {
        return _coefficients.Length;
    }

    public double CalculatePolynomial(int value)
    {
        double result = 0;
        for (int i = 0; i < _coefficients.Length; i++)
        {
            result += _coefficients[i] * Math.Pow(value, i);
        }
        return result;
    }

    public StringBuilder PrintPolynomial()
    {
        StringBuilder polynomialPrint = new StringBuilder(8 * _coefficients.Length);
        for (int i = 0; i < _coefficients.Length; i++)
        {
            string template;
            if (i > 1)
            {
                template = string.Format(_coefficients[i] > 0 ? " + {0}x^{1}" : " {0}x^{1}", _coefficients[i], i);
            }
            else
            {
                if (i == 1)
                {
                    template = string.Format(_coefficients[i] > 0 ? " + {0}x" : " {0}x", _coefficients[i]);
                }
                else
                {
                    template = string.Format(_coefficients[i] > 0 ? "{0}" : " {0}", _coefficients[i]);
                }              
            }
          polynomialPrint.Append(template);
        }
        return polynomialPrint;
    }
    //Operation addition
    public static Polynomial operator +(Polynomial firstPolynomial, IPolynomial secondPolynomial)
    {
        int maxPowerPolynomial = Math.Max(firstPolynomial.GetPowerPolinomial(), secondPolynomial.GetPowerPolinomial());
        Polynomial polynomial = new Polynomial(maxPowerPolynomial);
        for (int i = 0; i < polynomial.GetPowerPolinomial(); i++)
        {
            if (firstPolynomial.GetPowerPolinomial() > i)
            {
                polynomial[i] = firstPolynomial[i];
            }
            if (secondPolynomial.GetPowerPolinomial() > i)
            {
                polynomial[i] += secondPolynomial[i];
            }
        }
        return polynomial;
    }
    //Operation subtraction
    public static Polynomial operator -(Polynomial firstPolynomial, IPolynomial secondPolynomial)
    {
        int maxPowerPolynomial = Math.Max(firstPolynomial.GetPowerPolinomial(), secondPolynomial.GetPowerPolinomial());
        Polynomial polynomial = new Polynomial(maxPowerPolynomial);
        for (int i = 0; i < polynomial.GetPowerPolinomial(); i++)
        {
            if (firstPolynomial.GetPowerPolinomial() > i)
            {
                polynomial[i] = firstPolynomial[i];
            }
            if (secondPolynomial.GetPowerPolinomial() > i)
            {
                polynomial[i] -= secondPolynomial[i];
            }
        }
        return polynomial;
    }
    //Operation multiplication
    public static Polynomial operator *(Polynomial firstPolynomial, IPolynomial secondPolynomial)
    {
        int powerPolynomial = firstPolynomial.GetPowerPolinomial() + secondPolynomial.GetPowerPolinomial() - 1;
        Polynomial polynomial = new Polynomial(powerPolynomial);
        for (int i = 0; i < firstPolynomial.GetPowerPolinomial(); i++)
        {
            for (int j = 0; j < secondPolynomial.GetPowerPolinomial(); j++)
            {
                polynomial[i + j] += firstPolynomial[i] * secondPolynomial[j];
            }
        }
        return polynomial;
    }
}

class Program
{
    static void Main()
    {
        Console.WriteLine("___Task2___");
        Console.WriteLine();

        Polynomial pol1 = new Polynomial(new double[] { 1, 6 });
        Polynomial pol2 = new Polynomial(new double[] { 3, 5, 2 });
        Console.WriteLine(pol1.PrintPolynomial());
        Console.WriteLine(pol2.PrintPolynomial());
        Console.WriteLine();

        Console.WriteLine("**Calculate polynomial**");
        Console.WriteLine("Result pol1, value=2: {0}",pol2.CalculatePolynomial(2));
        Console.WriteLine();

        Console.WriteLine("Operation multiplication");
        Polynomial pol3 = pol1 * pol2;
        Console.WriteLine(pol3.PrintPolynomial());
        Console.WriteLine();

        Console.WriteLine("Operation subtraction");
        Polynomial pol4 = pol1 - pol2;
        Console.WriteLine(pol4.PrintPolynomial());
        Console.WriteLine();

        Console.WriteLine("Operation addition");
        Polynomial pol5 = pol1 + pol2;
        Console.WriteLine(pol5.PrintPolynomial());
        Console.WriteLine();
        
    }
}

